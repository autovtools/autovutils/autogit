from setuptools import setup, find_packages

VERSION = "0.0.0"
setup(
    name="autogit",
    version=VERSION,
    packages=find_packages(where="src"),
    install_requires=[
        "python-gitlab",
        "GitPython",
        "paramiko",
        "pydantic",
        "pycryptodome"
    ],  
)
# TODO: dependencies
