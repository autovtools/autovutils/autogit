from typing import Optional, List
from pydantic import BaseModel # pylint: disable=no-name-in-module
from pydantic import validator

import base64
import hashlib

class KeyModel(BaseModel):
    # Not actually optional
    title: Optional[str]
    key: str
    can_push: bool = True

    def get_fingerprint(self):
        """Return the OpenSSH-style MD5 fingerprint for this key

        https://stackoverflow.com/questions/6682815/deriving-an-ssh-fingerprint-from-a-public-key-in-python
        """
        # Format: ssh-rsa XXXX comments
        data = self.key.split()[1].encode()
        digest = hashlib.md5(base64.b64decode(data)).hexdigest()
        return ":".join(a+b for a,b in zip(digest[::2], digest[1::2]))
