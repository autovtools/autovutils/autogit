from typing import Optional, List
from pydantic import BaseModel # pylint: disable=no-name-in-module
from pydantic import validator

# https://docs.gitlab.com/ce/api/deploy_tokens.html#project-deploy-tokens
# vs
# https://docs.gitlab.com/ee/api/resource_access_tokens.html
class TokenModel(BaseModel):
    _scopes = [
        #"api",
        #"read_api",
        "read_repository",
        # there is no write_repository https://docs.gitlab.com/ce/api/deploy_tokens.html#create-a-project-deploy-token
        "read_registry",
        "write_registry",
        "read_package_registry",
        "write_package_registry",
    ]
    name: Optional[str]
    scopes: List[str]

    @validator("scopes", each_item=True)
    def validate_scopes(cls, scope): #pylint: disable=no-self-argument
        if scope not in cls._scopes:
            raise ValueError(f"{scope} not in {cls._scopes}")
        return scope
