from typing import Optional, Union

import getpass
from pydantic import BaseModel, validator, root_validator #pylint: disable=no-name-in-module

class ConfigModel(BaseModel):
    private_token: Optional[str]
    gitlab_url: str

    gitlab_cert: Optional[Union[bool, str]] = True
    deploy_ssh_key: Optional[str]

    password_len: Optional[int] = 64
    local_repos_dir: str = "/opt/autogit/"
    mirror_group: str = "mirror"
    submodule_mirror_group: str = "_submodules"
    # True if we should not attempt to pull from origin for normal projects.
    # Set when using autogit to replicate local code to a 'read-only' instance.
    push_only: bool = False

    admin_user: Optional[str]
    admin_password: Optional[str]

    # gitlab.com is protected from bots by cloudflare, so support bypassing the browser checks
    # https://github.com/venomous/cloudscraper
    # NOTE: disabled because it doesn't work
    #bypass_cloudflare: bool = False

    @root_validator
    def validate_model(cls, values): #pylint: disable=no-self-argument
        if values.get("private_token") is None:
            private_token = getpass.getpass(prompt="private_token: ")
            values["private_token"] = private_token
        return values
