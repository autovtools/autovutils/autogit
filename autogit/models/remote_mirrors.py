from pathlib import PurePosixPath
from urllib.parse import urlparse

from pydantic import BaseModel # pylint: disable=no-name-in-module
from pydantic import validator

class RemoteMirrorModel(BaseModel):
    url: str
    enabled: bool = True
    only_protected_branches: bool = False
    keep_divergent_refs: bool = False

    # can use with global replica_mirror
    start_depth: int = 0

    # Don't set these
    auth_method: str = "ssh_public_key"
    ssh_known_hosts: str = ""
    password: str = ""

    @validator("url")
    def validate_url(cls, url): # pylint: disable=no-self-argument
        parsed = urlparse(url)
        if parsed.scheme != "ssh":
            # ssh only for security
            raise ValueError(f"[-] Only ssh:// push_mirror urls are supported")
        # A common mistake is ssh://X:/ (second colon is bad)
        if not PurePosixPath(parsed.path).is_absolute():
            raise ValueError(f"[-] Bad path in ssh url: {url} | should be ssh://git@HOST/foo/bar/foobar.git")
        return url
