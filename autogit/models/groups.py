from typing import Optional

from pydantic import BaseModel #pylint: disable=no-name-in-module
from pydantic import validator, root_validator

import gitlab

class GroupModel(BaseModel):
    _gitlab_visibility = {
        "PRIVATE": gitlab.VISIBILITY_PRIVATE,
        "INTERNAL": gitlab.VISIBILITY_INTERNAL,
        "PUBLIC": gitlab.VISIBILITY_PUBLIC,
    }

    name: str

    parent_id: Optional[int]
    visibility: Optional[str]
    path: Optional[str]

    # custom
    protected: bool = False

    @validator("visibility")
    def validate_visibility(cls, visibility): #pylint: disable=no-self-argument
        if visibility.upper() in cls._gitlab_visibility:
            return visibility.lower()
        else:
            raise ValueError(f"{visibility.uppper()} not in {list(cls._gitlab_visibility)}")

    @root_validator
    def validate_model(cls, values): #pylint: disable=no-self-argument
        if values.get("path") is None:
            values["path"] = values["name"]
        return values
