import secrets
from typing import Optional

from pydantic import BaseModel #pylint: disable=no-name-in-module
from pydantic import root_validator

import gitlab

class MemberModel(BaseModel):
    _gitlab_access = {
        "NO_ACCESS": gitlab.NO_ACCESS,
        "MINIMAL_ACCESS": gitlab.MINIMAL_ACCESS,
        "GUEST_ACCESS": gitlab.GUEST_ACCESS,
        "REPORTER_ACCESS": gitlab.REPORTER_ACCESS,
        "DEVELOPER_ACCESS": gitlab.DEVELOPER_ACCESS,
        "MAINTAINER_ACCESS": gitlab.MAINTAINER_ACCESS,
        "OWNER_ACCESS": gitlab.OWNER_ACCESS,
    }
    user_id: int
    access_level_name: Optional[str] = "REPORTER_ACCESS"
    access_level: Optional[int]

    @root_validator
    def validate_model(cls, values): #pylint: disable=no-self-argument
        level = values["access_level_name"].upper()
        values["access_level"] = cls._gitlab_access[level]
        del values["access_level_name"]
        return values
        
