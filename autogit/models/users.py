import secrets
from typing import Optional

from pydantic import BaseModel #pylint: disable=no-name-in-module
from pydantic import root_validator

class UserModel(BaseModel):
    _domain = "localhost"
    _password_len = 64

    username: str

    password: Optional[str]
    email: Optional[str]
    name: Optional[str]

    @root_validator
    def validate_model(cls, values): #pylint: disable=no-self-argument
        if values.get("name") is None:
            values["name"] = values["username"]
        if values.get("email") is None:
            values["email"] = f"{values['username']}@{cls._domain}"
        if values.get("password") is None:
            values["password"] = secrets.token_urlsafe(cls._password_len)
        return values
