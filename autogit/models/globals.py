from typing import Optional, List, Dict

from pydantic import BaseModel #pylint: disable=no-name-in-module
from pydantic import root_validator

from .keys import KeyModel
from .remote_mirrors import RemoteMirrorModel

class DeployModel(BaseModel):
    keys: Optional[Dict[str, KeyModel]] = {}

class GlobalModel(BaseModel):
    allow_local_requests: Optional[List[str]] = {}
    replica_push_mirror: Optional[List[RemoteMirrorModel]] = {}

    deploy: Optional[DeployModel] = DeployModel()
