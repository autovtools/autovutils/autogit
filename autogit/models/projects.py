from typing import Optional

from pydantic import BaseModel #pylint: disable=no-name-in-module
from pydantic import validator

import gitlab

#DEFAULT_VISIBILITY = "private"
class ProjectModel(BaseModel):
    _gitlab_visibility = {
        "PRIVATE": gitlab.VISIBILITY_PRIVATE,
        "INTERNAL": gitlab.VISIBILITY_INTERNAL,
        "PUBLIC": gitlab.VISIBILITY_PUBLIC,
    }
    name: str
    namespace_id: Optional[int]
    visibility: Optional[str]

    @validator("visibility")
    def validate_visibility(cls, visibility): #pylint: disable=no-self-argument
        if visibility is None:
            visibility = DEFAULT_VISIBILITY
        if visibility.upper() in cls._gitlab_visibility:
            return visibility.lower()
        else:
            raise ValueError(f"{visibility.uppper()} not in {list(cls._gitlab_visibility)}")
