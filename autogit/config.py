# pip install getpass pycryptodome
import struct
from pathlib import Path

import getpass
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Protocol.KDF import scrypt

SALT_LEN = 32
IV_LEN = 16
KEY_LEN = 32
GMAC_LEN = 16

# https://pycryptodome.readthedocs.io/en/latest/src/protocol/kdf.html?highlight=pbkdf#scrypt-func
# More secure == more slow; too slow is annoying
SCRYPT_COST = (1<<20)
SCRYPT_BLOCK_SIZE = 8
SCRYPT_PARALLEL = 1

# SALT | IV | DATA | GMAC
ENC_HEADER = f"{SALT_LEN}s{IV_LEN}s"
ENC_FMT = "{}s".join([f"{ENC_HEADER}", f"{GMAC_LEN}s"])

def derive_key(password, salt):
    return scrypt(password, salt, KEY_LEN, SCRYPT_COST, SCRYPT_BLOCK_SIZE, SCRYPT_PARALLEL)

def encrypt_data(plaintext):
    password = getpass.getpass(prompt='Encryption password:')
    salt = get_random_bytes(SALT_LEN)
    iv = get_random_bytes(IV_LEN)
    key = derive_key(salt, password)

    cipher = AES.new(key, AES.MODE_GCM, nonce=iv)
    header = struct.pack(ENC_HEADER, salt, iv)
    cipher.update(header)
    encrypted, gmac = cipher.encrypt_and_digest(plaintext)

    return header + encrypted + gmac

def unpack_data(encrypted):
    header_len = struct.calcsize(ENC_HEADER)
    data_len = len(encrypted) - header_len - GMAC_LEN

    return struct.unpack(ENC_FMT.format(data_len), encrypted)
    
def decrypt_data(encrypted):
    salt, iv, encrypted, gmac = unpack_data(encrypted)
    password = getpass.getpass(prompt='Decryption password:')
    key = derive_key(salt, password)

    cipher = AES.new(key, AES.MODE_GCM, nonce=iv)
    cipher.update(salt + iv)
    return cipher.decrypt_and_verify(encrypted, gmac)
    
# Helpers for files
def encrypt_file(in_path, out_path=None):
    if out_path is None:
        out_path = in_path
    plaintext = Path(in_path).read_bytes()
    encrypted = encrypt_data(plaintext)
    Path(out_path).write_bytes(encrypted)

def decrypt_file(in_path, out_path=None):
    if out_path is None:
        out_path = in_path
    encrypted = Path(in_path).read_bytes()
    plaintext = decrypt_data(encrypted)
    Path(out_path).write_bytes(plaintext)

