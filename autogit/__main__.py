import re
import copy
import time
import argparse
import secrets
import shlex
import tempfile
from pathlib import Path, PurePosixPath
from contextlib import contextmanager
from urllib.parse import urlparse, quote_plus

import gitlab
import git
import requests
import getpass
from paramiko.rsakey import RSAKey
from git import Repo

from autogit.models import *
from autogit.config import encrypt_data, decrypt_data

import yaml
#import cloudscraper
#https://stackoverflow.com/questions/37200150/can-i-dump-blank-instead-of-null-in-yaml-pyyaml/37201633#37201633
def represent_none(self, _):
    return self.represent_scalar('tag:yaml.org,2002:null', '')

yaml.add_representer(type(None), represent_none)

class AutoGit():
    def __init__(self, config=None, config_path=None, encrypted=False):
        if config is None:
            config = self.load_config(config_path, encrypted=encrypted)
        else:
            config = config
        self.config = ConfigModel(**config)
        # id -> KeyModel
        self.global_keys ={}

        self.replica_curr = {}
        self.replica_spec =  self.replica_curr

        self.spec_curr = None

        self.gl = gitlab.Gitlab(
            self.config.gitlab_url,
            private_token=self.config.private_token,
            ssl_verify=self.config.gitlab_cert
        )

    def _get_token(self, html, name="csrf-token", field="content"):
        if 'Checking your browser before accessing' in html:
            raise Exception("[-] Failed to get UI token: am robot")

        regex = re.compile(f'name="{name}".*{field}="([^"]+)"')
        token = None
        for line in html.split('\n'):
            match = regex.search(line)
            if match:
                #print(f"match: {match.group(1)}")
                return match.group(1)
        print(html)
        raise Exception(f"[-] Failed to find {name} in response")

    def add_replica_group(self, group):
        old_curr = self.replica_curr
        self.replica_curr.setdefault("groups", {})
        tmp = {}
        self.replica_curr["groups"][group.name] = tmp
        self.replica_curr = tmp
        return old_curr

    def add_replica_project(self, project):
        self.replica_curr.setdefault("projects", {})
        self.replica_curr["projects"][project.name] = {}
        return self.replica_curr["projects"][project.name]

    @contextmanager
    def web_ui_session(self):
        self.require_admin_creds()
        sign_in_url = f"{self.config.gitlab_url}/users/sign_in"
        session_class = requests.Session
        #if self.config.bypass_cloudflare:
        #    print("[*] Attempting Cloudflare bypass...")
        #    session_class = cloudscraper.CloudScraper
        with session_class() as session:
            session.verify = self.config.gitlab_cert
            csrf_token = self._get_token(session.get(sign_in_url).text)
            #csrf_token = self._get_token(session.get(sign_in_url).text, name="authenticity_token", field="value")
            data = {
                "user[login]": self.config.admin_user,
                "user[password]": self.config.admin_password,
                "authenticity_token": csrf_token
            }
            response = session.post(sign_in_url, data=data)
            if not response.ok:
                #print(response.text)
                raise Exception(f"[-] Log in failed")
            yield session

    def password_to_token(self):
        """A brittle helper to bootstrap the intial access token by 'logging in' and submitting the 'new token' form

        See https://gist.github.com/gpocentek/bd4c3fbf8a6ce226ebddc4aad6b46c0a
        """
        with self.web_ui_session() as session:
            # Now session has a _gitlab_session cookie
            tokens_url = f"{self.config.gitlab_url}/-/profile/personal_access_tokens"

            token_name = "api_bootstrap_token"
            response = session.get(tokens_url)
            if not response.ok:
                raise Exception(f"[-] Get tokens failed")

            csrf_token = self._get_token(response.text)
            data = {
              "personal_access_token[name]": token_name,
              "personal_access_token[scopes][]": "api",
              "authenticity_token": csrf_token
            }

            response = session.post(tokens_url, data=data)
            if not response.ok:
                raise Exception(f"[-] Token creation failed")
                
            private_token = self._get_token(
                response.text,
                name="created-personal-access-token",
                field="value"
            )
            print(f"[+] Created {token_name}: {private_token}")
            return private_token

    def global_deploy_key(self, key):

        # You can't create a global deploy key via the API...
        # so 'manually' submit the HTML form (:
        # https://docs.gitlab.com/ce/api/deploy_keys.html
        # Obviously brittle
        deploy_keys_post_url = f"{self.config.gitlab_url}/admin/deploy_keys"
        deploy_keys_get_url = f"{deploy_keys_post_url}/new"
        with self.web_ui_session() as session:
            csrf_token = self._get_token(session.get(deploy_keys_get_url).text)
            data = {
                "deploy_key[title]": key.title,
                "deploy_key[key]": key.key,
                "authenticity_token": csrf_token
            }
            response = session.post(deploy_keys_post_url, data=data)
            if not response.ok:
                raise Exception(f"[-] Failed to create global deploy key {key.title}: {response.status_code} | {deploy_keys_post_url}")
        keys = self.gl.deploykeys.list(all=True)
        # ssh-rsa XXXX comment
        pubkey_blob = key.key.split()[1]
        key_obj = None
        for k in keys:
            blob = k.key.split()[1]
            if pubkey_blob == blob:
                key_obj = k
                break
        if key_obj is None:
            print(f"[-] Failed to look up global deploy key {key.title} ({key.get_fingerprint()})")
        else:
            print(f"[+] Created global deploy key: {key.title}")
            self.global_keys[key_obj.id] = key

    def delete_global_deploy_key(self, key):

        # You can't delete a global deploy key via the API...
        # so 'manually' submit the HTML form (:
        # https://docs.gitlab.com/ce/api/deploy_keys.html
        # Obviously brittle
        deploy_keys_url = f"{self.config.gitlab_url}/admin/deploy_keys"
        delete_url = f"{deploy_keys_url}/{key.id}"
        with self.web_ui_session() as session:
            csrf_token = self._get_token(session.get(deploy_keys_url).text)
        
            data = {
                "_method": "delete",
                "authenticity_token": csrf_token
            }
            response = session.post(delete_url, data=data)
            if not response.ok:
                print(f"[-] Failed to delete global deploy key {key.title} ({key.id}): {response.status_code} | {delete_url}")

    def get_group(self, group_name, parent_id=None):

        group_list = []
        if parent_id is not None:
            try:
                parent_group = self.gl.groups.get(parent_id)
                subgroup_list = parent_group.subgroups.list(search=group_name, all=True)
                group_list = [self.gl.groups.get(group.id) for group in subgroup_list]
            except Exception as e:
                print(f"[!] Exception getting group {group_name}: {e}")
        else:
            group_list = self.gl.groups.list(search=group_name)

        return self.found(self.filter(group_list, name=group_name))

    def get_project(self, project_name, parent_id=None):
        project_list = []
        if parent_id is not None:
            try:
                parent_group = self.gl.groups.get(parent_id)
                project_list = parent_group.projects.list(search=project_name, all=True)
                project_list = [self.gl.projects.get(project.id) for project in project_list]
            except Exception as e:
                print(f"[!] Exception getting project {project_name}: {e}")
        else:
            project_list = self.gl.projects.list(search=project_name, all=True)
            project_list = [project for project in project_list if project.namespace['id'] == parent_id]

        return self.found(self.filter(project_list, name=project_name))

    def get_user(self, username):
        user = self.gl.users.list(username=username)
        return self.found(self.filter(user, username=username))

    def filter(self, objs, first=True, **kwargs):
        #print(f"[*] Filter [{objs}] by {kwargs}")
        res = []
        for obj in objs:
            if all(getattr(obj, key) == val for key, val in kwargs.items()):
                if first:
                    return obj
                else:
                    res.append(obj)
        if first:
            return None
        else:
            print(f"[*] Filtered: {res}")
            return res
        
    def create_groups(self, groups, parent_id=None):
        if groups is None:
            return

        for group_name, group_data in groups.items():
            print("[*] ...")
            if group_data is None:
                group_data = {}
            #print(f"\n[*] Visit group {group_name} (parent: {parent_id})")
            group = self.get_group(group_name, parent_id=parent_id)
            visibility = group_data.get("visibility")
            if visibility is None:
                if parent_id is not None:
                    visibility = self.gl.groups.get(parent_id).visibility
                    print(f"[+] Inherit {visibility} from {parent_id}")
                else:
                    visibility = "private"

            if group is None:
                # TODO: This is bad
                group_model = GroupModel(
                    name=group_name,
                    parent_id=parent_id,
                    visibility=visibility
                )
                group_dict = group_model.dict(exclude_none=True)
                print(f"[+] Create Group {group_model.name}")
                group = self.gl.groups.create(group_dict)

            old_curr = self.add_replica_group(group)
            # Create tokens
            self.add_deploytokens(group, group_data.get("tokens", {}))
            # Create projects
            self.create_projects(group_data.get("projects", None), group.id)
            # Recurse
            self.create_groups(group_data.get("groups", None), parent_id=group.id)
            self.replica_curr = old_curr

            # There is no such thing as a 'group deploy key'
            # https://docs.gitlab.com/ce/api/deploy_keys.html#adding-deploy-keys-to-multiple-projects
            # So just apply any group-level keys in the config to all projects under the group
            # TODO: Warn users we do this
            group_keys = group_data.get("keys", {})
            if group_keys:
                for project in group.projects.list(include_subgroups=True):
                    # Get a real project ref
                    project = self.gl.projects.get(project.id)
                    self.add_keys(project, group_keys)

    @contextmanager
    def tmp_token(self, obj, scopes=["read_repository"]):
        name = f"{self.__class__.__name__}.token.{obj.name}.tmp"
        token = TokenModel(
            name=name,
            scopes=scopes,
        )
        created = None
        try:
            created = obj.deploytokens.create(token.dict())
            print(f"[+] Created tmp token: {name}")
            yield created
        finally:
            if created is not None:
                print(f"[+] Deleted tmp token: {name}")
                created.delete()

    @contextmanager
    def tmp_key(self, obj, can_push=True, bits=4096):
        name = f"{self.__class__.__name__}.key.{obj.name}.tmp"
        pkey = RSAKey.generate(bits)
        pub = f"{pkey.get_name()} {pkey.get_base64()}"

        key = KeyModel(
            title=name,
            key=pub,
            can_push=can_push,
        )
        created = None
        try:
            created = obj.keys.create(key.dict())
            print(f"[+] Created tmp key: {name}")

            with tempfile.NamedTemporaryFile() as tmp:
                print(f"[+] Wrote {name} to {tmp.name}")
                pkey.write_private_key_file(tmp.name)
                yield tmp.name
        except Exception as e:
            print(f"[-] git operation failed: {e}")
            # TODO
            #import time
            #time.sleep(600)
            raise
        finally:
            if created is not None:
                print(f"[+] Deleted tmp key: {name}")
                created.delete()
        
    def mirror_submodule(self, remote_url):
        # 'Securely' parse the submodule URL
        # WARNING: probably wrong in a dangerous way
        parsed = urlparse(remote_url)
        # Drop the .git from the end of the url, if present
        path = PurePosixPath(parsed.path.rsplit(".git", 1)[0])
        # Reconstruct clean URL with new path (also drops any weirdness like query params
        remote_url = f"{parsed.scheme}://{parsed.netloc}{parsed.path}"

        # Validate
        if not path.is_absolute():
            raise ValueError(f"Bad submodule URL: {remote_url} | {path} is not absolute")
        if not path.name:
            raise ValueError(f"Bad submodule URL: {remote_url} | no project name found")
        # Reverse the list of parent dirs ('/' first) and drop the '/'
        # It's an absolute path and pathlib normalizes, so the first entry _should_ be /
        parents = [p.name for p in list(PurePosixPath(path).parents)[::-1][1:]]

        if parents and parents[0] == self.config.submodule_mirror_group:
            raise ValueError("Bad submodule URL: {remote_url} | token {self.config.submodule_mirror_group} is reserved")

        path_with_namespace = str(PurePosixPath(self.config.mirror_group) / self.config.submodule_mirror_group / PurePosixPath(*parents) / path.name)
        print(f"[*] {remote_url} -> {path_with_namespace}")
        # Kinda weird, but the easiest way to recursively handle everything is to just build a valid spec + handle it like normal

        # Abuse mutable dicts to make this a little cleaner
        group_data = {}
        # Conents under an imaginary root-level 'groups' key
        group_spec = {
            self.config.mirror_group: {
                "groups": {
                    self.config.submodule_mirror_group: group_data
                }
            }
        } 
        # The parent paths are all subgroups under mirror_group
        for parent in parents:
            # mutate
            group_data["groups"] = {
                parent: {}
            }
            # update ref
            group_data = group_data["groups"][parent]

        # Finally, add the 'projects' key with the basename of the url as the project name
        group_data["projects"] = {
            path.name: {
                "remote_repo": remote_url,
                "pull_mirror": True,
            }
        }
        self.create_groups(group_spec)
        # Find the project we created
        # If you have a billion projects, this might not be great
        found = self.found(self.filter(self.gl.projects.list(all=True), path_with_namespace=path_with_namespace))
        if found is None:
            raise Exception(f"[-] Failed to look up mirror submodule: {path_with_namespace}")
        print(f"[+] Mirrored submodule to {found.ssh_url_to_repo}")
        return found.ssh_url_to_repo

    def create_pull_mirror(self, project, project_data):
        local = project_data.get(
            "local_repo",
            Path(self.config.local_repos_dir) / project.path_with_namespace
        )
        remote = project_data["remote_repo"]
        repo = None
        if not Path(local).is_dir():
            if self.config.push_only:
                print(f"[-] {local} not found: skipping")
                return
            else:
                Path(local).mkdir(parents=True, exist_ok=True)
                repo = Repo.clone_from(remote, local, mirror=True)
                print(f"[+] Cloned from {remote} to {local}")
        else:
            repo = Repo(local)
        print("[*] ...")

        # Fetch
        #print("[*] Fetching remote...")
        #repo.git.fetch(all=True, prune=True)

        # Loop over all the 
        #tracking = set([branch.tracking_branch() for branch in repo.branches])
        #for ref in repo.remotes.origin.refs:
        #    if ref not in tracking:
        #        repo.create_head(ref.remote_name, ref).set_tracking_branch(ref).checkout() 

        if not self.config.push_only:
            print(f"[+] pull_mirror: {remote} -> {local}")
            print("[*] Pulling remote...")
            repo.remotes.origin.update(prune=True)
        # Pull
        #repo.remotes.origin.pull(all=True, tags=True)

        #print("[*] Processing submodules...")
        # Update submodules
        #repo.submodule_update(recursive=True)

        actual_urls = []
        remote_push_name = project_data.get("remote_push_name", "autogit_mirror_origin")
        try:
            for submodule in repo.submodules:
                print(f"[*] Found submodule: {submodule.url}")
                mirror_url = self.mirror_submodule(submodule.url)
                actual_urls.append(submodule.url)

                '''
                with submodule.config_writer() as writer:
                    writer.set('url', mirror_url)
                print(f"[*] Set submodule url: {mirror_url}")
                '''

            repo.create_remote(remote_push_name, project.ssh_url_to_repo, mirror="push")
            print(f"[*] Create remote {remote_push_name} for {project.ssh_url_to_repo}")
            # Authorize a temp push key
            with self.tmp_key(project) as id_rsa:
                ssh_cmd = f"ssh -o IdentitiesOnly=yes -i {shlex.quote(id_rsa)}"
                # Push to the mirrored repo using our temporary key
                with repo.git.custom_environment(GIT_SSH_COMMAND=ssh_cmd):
                    print(f"[*] Push to {remote_push_name} ({project.ssh_url_to_repo})")
                    repo.remote(remote_push_name).push(mirror=True)
                
            print(f"[*] Pushed {local} to {project.path_with_namespace}")
        finally:
            # Restore submodules
            try:
                for submodule, actual_url in zip(repo.submodules, actual_urls):
                    print(f"[*] Restore submodule: {actual_url}")
                    '''
                    with submodule.config_writer() as writer:
                        writer.set('url', actual_url)
                    '''
            except Exception as e:
                print(f"[-] Error fixing submodules: {e}")

            # Delete temporary remote
            if repo.remote(remote_push_name).exists():
                repo.delete_remote(remote_push_name)

        default_branch = None
        try:
            # gitlab does not set the default branch correctly; fix it to match upstream
            # Example input: 'ref: refs/heads/dev\tHEAD\nda39a3ee5e6b4b0d3255bfef95601890afd80709\tHEAD'
            default_branch = repo.git.ls_remote("origin", "HEAD", symref=True).split('\t')[0].split('refs/heads/')[1]
        except Exception as e:
            print(f"[-] Failed to get default branch: {e}")

        if default_branch is not None and project.default_branch != default_branch:
            project.default_branch = default_branch
            project.save()
            print(f"[+] Set default_branch to {default_branch} for {project.path_with_namespace}")

    def _local_repo_path(self, project):
        # WARNING: Path traversal, probably
        # https://stackoverflow.com/questions/3812849/how-to-check-whether-a-directory-is-a-sub-directory-of-another-directory
        local_repos_dir = Path(self.config.local_repos_dir)
        local_repo = local_repos_dir / project.path_with_namespace
        if local_repos_dir not in local_repo.parents:
            raise RuntimeError(f"[!!!] Path traversal detected: {local_repo} not in {local_repos_dir}")
        return local_repo
                
    def push_project(self, project):
        local_repo = self._local_repo_path(project)
        if local_repo.is_dir():
            repo = Repo(str(local_repo))

            # TODO self.config.push_remote
            remote_push_name = "origin"

            try:
                if self.config.push_only:
                    remote_push_name = "autogit_push_only_origin"
                    repo.create_remote(remote_push_name, project.ssh_url_to_repo, mirror="push")
                    print(f"[*] Create remote {remote_push_name} for {project.ssh_url_to_repo}")
                # Authorize a temp push key
                with self.tmp_key(project) as id_rsa:
                    ssh_cmd = f"ssh -o IdentitiesOnly=yes -i {shlex.quote(id_rsa)}"
                    # Push to the mirrored repo using our temporary key
                    print(f"GIT_SSH_COMMAND={ssh_cmd}")
                    with repo.git.custom_environment(GIT_SSH_COMMAND=ssh_cmd):
                        try:
                            repo.remote(remote_push_name).push(mirror=True)
                        except Exception as e:
                            print(f"[-] Error pushing {project.path_with_namespace}: {e}")
            finally:
                if self.config.push_only:
                    # Delete temporary remote
                    if repo.remote(remote_push_name).exists():
                        repo.delete_remote(remote_push_name)
                
            print(f"[+] Restored project contents from: {local_repo}")

    def pull_project(self, project):
        local_repo = self._local_repo_path(project)
        # Authorize a temp pull key
        with self.tmp_key(project, can_push=False) as id_rsa:
            ssh_cmd = f"ssh -o IdentitiesOnly=yes -i {shlex.quote(id_rsa)}"
            if local_repo.is_dir():
                repo = Repo(str(local_repo))
                with repo.git.custom_environment(GIT_SSH_COMMAND=ssh_cmd):
                    # Pull with our tmp key
                    repo.remotes.origin.update(prune=True)
            else:
                local_repo.parent.mkdir(parents=True, exist_ok=True, mode=0o700)
                env = {"GIT_SSH_COMMAND": ssh_cmd}
                repo = Repo.clone_from(project.ssh_url_to_repo, str(local_repo), mirror=True, env=env)
        print(f"[+] Mirrored {project.path_with_namespace} to: {local_repo}")
        
    def get_push_mirror_keys(self, project):
        # The ssh public keys a push mirror uses are randomly generated and not returned or accessible via the API :/
        # https://docs.gitlab.com/ee/api/remote_mirrors.html
        # The intended way to access them is to click a 'copy to clipboard' button under /settings/repository#js-push-remote-settings
        settings_url = f"{self.config.gitlab_url}/{project.path_with_namespace}/-/settings/repository#js-push-remote-settings"
        url_pattern = re.compile(".*mirror_repository_url_cell[^>]*>([^<]*)<.*")
        key_pattern = re.compile('.*copy_public_key_button.*data-clipboard-text="([^"]*)"')
        with self.web_ui_session() as session:
            response = session.get(settings_url)
            if not response.ok:
                raise Exception(f"[-] Failed to get push mirror settings: {response.status_code}")
            # WARNING: Brittle
            urls = []
            keys = []
            url = None
            for line in response.text.splitlines():
                match = url_pattern.match(line)
                if match:
                    url = match.group(1)
                    print(f"[*] {url}")
                    parsed = urlparse(url)
                    # Only return ssh urls
                    if parsed.scheme != "ssh":
                        url = None
                match = key_pattern.match(line)
                if match:
                    # Keep lists in sync / same length
                    # When we find a key, assume it belongs to the last ssh url we saw
                    if url is not None:
                        urls.append(url)
                        keys.append(match.group(1))
                        url = None
        return {url: key for url, key in zip(urls, keys)}

    def get_remote_mirrors(self, project, project_data):
        remote_mirrors = []
        if project_data is not None:
            project_data.get("remote_mirrors", [])
        for mirror in self.spec_global_curr.replica_push_mirror:
            new_mirror = mirror.dict()
            # WARNING: Probably dangerous
            if not new_mirror["url"].endswith("/"):
                new_mirror["url"] += "/"
            start_depth = new_mirror.get("start_depth", 0)
            subpath = PurePosixPath('').joinpath(*PurePosixPath(project.path_with_namespace).parts[start_depth:])  
            new_mirror["url"] += str(subpath)
            remote_mirrors.append(new_mirror)
        return remote_mirrors

    def create_projects(self, projects, namespace_id):
        if projects is None:
            return

        for project_name, project_data in projects.items():
            project = self.get_project(project_name, parent_id=namespace_id)
            pull_mirror = False
            if project_data is not None:
                pull_mirror = project_data.get("pull_mirror", False)
            if project is None:
                if project_data is None:
                    project_data = {}
                visibility = project_data.get("visibility")
                if visibility is None:
                     visibility = self.gl.groups.get(namespace_id).visibility

                project_model = ProjectModel(
                    name=project_name,
                    namespace_id=namespace_id,
                    visibility=visibility
                )
                project_dict = project_model.dict(exclude_none=True)
                print(f"[+] Create Project {project_model.name}")
                project = self.gl.projects.create(project_dict)

            replica_data = self.add_replica_project(project)

            if pull_mirror:
                self.create_pull_mirror(project, project_data)
            else:
                self.push_project(project)
                if not self.config.push_only:
                    self.pull_project(project)

            # Setup remote mirrors
            # https://python-gitlab.readthedocs.io/en/latest/gl_objects/remote_mirrors.html
            # TODO: API does not support creating push mirrors with ssh key auth
            remote_mirrors = self.get_remote_mirrors(project, project_data)
            urls = []
            if remote_mirrors:
                mirrors = self.get_push_mirror_keys(project)
                for remote_mirror_data in remote_mirrors:
                    remote_mirror = RemoteMirrorModel.parse_obj(remote_mirror_data)
                    if remote_mirror.url in mirrors:
                        print(f"[*] Found push_mirror: {remote_mirror.url}")
                    else:
                        self.add_push_mirror(project, remote_mirror)
                        print(f"[+] Push mirror {project.path_with_namespace} to {remote_mirror.url}")

                # Pull the mirror list (including keys) from the web UI
                mirrors = self.get_push_mirror_keys(project)
                replica_data["keys"] = {}
                for url, key in mirrors.items():
                    replica_data["keys"][url] = {
                        "key": mirrors[url],
                        "can_push": True,
                    }

            if isinstance(project_data, dict):
                # Add keys
                self.add_keys(project, project_data.get("keys", {}))
                # Add tokens
                self.add_deploytokens(project, project_data.get("tokens", {}))

            # Enable all keys
            # TODO: Warn the user we do this
            for key_id, key in self.global_keys.items():
                print(f"\t[*] Enable deploykey {key.title} for {project.path_with_namespace}")
                project.keys.enable(key_id)
                if key.can_push:
                    print(f"\t[+] Grant write permission to {key.title} for {project.path_with_namespace}")
                    self.grant_write_to_key(project, key_id, key)

            # If we didn't end up adding anything to the project's key in the YAML, leave it empty
            if not replica_data:
                self.replica_curr["projects"][project.name] = None

    def grant_write_to_key(self, project, key_id, key):
        post_url = f"{self.config.gitlab_url}/{project.path_with_namespace}/-/deploy_keys/{key_id}"
        get_url = f"{post_url}/edit"
        with self.web_ui_session() as session:
            csrf_token = self._get_token(session.get(get_url).text)
            # Weird structure, but this is what the browser sends
            data = {
                "_method": "patch",
                "authenticity_token": csrf_token,
                "deploy_key[title]": key.title,
                "deploy_key[fingerprint]": key.get_fingerprint(),
                "deploy_key[deploy_keys_projects_attributes][0][can_push]": [0, 1],
            }

            response = session.post(post_url, data=data)
            
            if not response.ok:
                print(f"[-] Failed to grant write permission to global deploy key {key.title} ({key_id}): {response.status_code} {post_url}")

            # It seems like we need to re-enable after an edit
            project.keys.enable(key_id)

    def add_push_mirror(self, project, remote_mirror):
        get_url = f"{self.config.gitlab_url}/{project.path_with_namespace}/-/settings/repository#js-push-remote-settings"
        post_url = f"{self.config.gitlab_url}/{project.path_with_namespace}/-/mirror"

        known_hosts_url = f"{post_url}/ssh_host_keys.json"
        params = {
            "ssh_url": remote_mirror.url,
            "compare_host_keys":""
        }

        with self.web_ui_session() as session:
            csrf_token = self._get_token(session.get(get_url).text)

            # Weird structure, but this is what the browser sends
            data = {
                "_method": "patch",
                "authenticity_token": csrf_token,
                "url": remote_mirror.url,
                "project[remote_mirrors_attributes][0][enabled]": "1" if remote_mirror.enabled else "0",
                "project[remote_mirrors_attributes][0][url]": remote_mirror.url,
                "project[remote_mirrors_attributes][0][only_protected_branches]": "0" if remote_mirror.only_protected_branches else "0",
                "project[remote_mirrors_attributes][0][keep_divergent_refs]": "1" if remote_mirror.keep_divergent_refs else "0",
                "project[remote_mirrors_attributes][0][auth_method]": remote_mirror.auth_method,
                "project[remote_mirrors_attributes][0][password]": remote_mirror.password,
            }

            known_hosts = None
            if remote_mirror.ssh_known_hosts:
                known_hosts = remote_mirror.ssh_known_hosts
            elif remote_mirror.auth_method == "ssh_public_key":
                while True:
                    print(f"[*] Fetching host key for {remote_mirror.url}...")
                    response = session.get(known_hosts_url, params=params)
                    if not response.ok:
                        print(f"[-] Failed to get host key from {known_hosts_url}")
                        return
                    if response.status_code == 204:
                        time.sleep(0.5)
                    elif response.status_code == 200:
                        break
                        
                known_hosts = response.json()["known_hosts"]
            
            #print(f"[*] known_hosts: {known_hosts}")
            data["project[remote_mirrors_attributes][0][ssh_known_hosts]"] = known_hosts

            response = session.post(post_url, data=data)
            if not response.ok:
                print(f"[-] Failed to add push mirror {remote_mirror.url} for {project.path_with_namespace}: {response.status_code}")

    def set_allow_local_requests(self, allow_list):
        get_url = f"{self.config.gitlab_url}/admin/application_settings/network"
        post_url = get_url

        with self.web_ui_session() as session:
            csrf_token = self._get_token(session.get(get_url).text)
            # Weird structure, but this is what the browser sends
            # TODO: Warn we nuke any exists settings
            data = {
                "_method": "patch",
                "authenticity_token": csrf_token,
                # Checked
                "application_setting[allow_local_requests_from_web_hooks_and_services]": "0",
                "application_setting[allow_local_requests_from_web_hooks_and_services]": "1",
                # Checked
                "application_setting[allow_local_requests_from_system_hooks][0]": "0",
                "application_setting[allow_local_requests_from_system_hooks][1]": "1",
    
                "application_setting[outbound_local_requests_allowlist_raw]": "\n".join(allow_list),
                "application_setting[dns_rebinding_protection_enabled][0]": "0",
                "application_setting[dns_rebinding_protection_enabled][1]": "1",
            }

            response = session.post(post_url, data=data)
            if not response.ok:
                print(f"[-] Failed to allow local requests for instance: {response.status_code}")
        print(f"[+] Allow local requests to {', '.join(allow_list)}")

    def add_keys(self, obj, keys):
        for title, key_data in keys.items():
            key_data.setdefault("title", title)
            key = KeyModel.parse_obj(key_data)
            try:
                found = self.filter(obj.keys.list(all=True), title=key.title)
                if found is not None and self.filter([found], key=key.key) is not None:
                    print(f"[+] Found key {title} for {obj.__class__.__name__} {obj.name}")
                elif found is not None:
                    # Title matched, key didn't
                    # Gitlab adds a comment at the end, so this might happen more often than needed
                    # (not an issue)
                    print(f"[!] Recreating conflicting key {title} for {obj.__class__.__name__} {obj.name}")
                    found.delete()
                else:
                    # No key with this title
                    obj.keys.create(key.dict())
                    print(f"[+] Create key {title} for {obj.__class__.__name__} {obj.name}")
            except Exception as e:
                print(f"[-] Failed to add key to {obj.__class__.__name__} {obj.name}: {e}")

    def add_deploytokens(self, obj, tokens):
        """
        Add deploy tokens to (Group|Project)
        """
        for name, token_data in tokens.items():
            token_data.setdefault("name", name)
            token = TokenModel(**token_data)
            try:
                found = self.filter(obj.deploytokens.list(all=True), name=token.name)
                if found is None:
                    # Token does not exist
                    created = obj.deploytokens.create(token.dict())
                    print(f"[+] Create token {name} for {obj.__class__.__name__} {obj.name} ({created.id}):{created.token}")
                else:
                    print(f"[+] Found token {name} for {obj.__class__.__name__} {obj.name}")

            except Exception as e:
                import traceback
                traceback.print_exc()
                print(f"[-] Failed to add token to {obj.__class__.__name__} {obj.name}: {e}")

    def create_users(self, spec):
        users = spec.get("users", None)
        if users is None:
            return
        for username, user_data in users.items():
            # Create user
            user = self.get_user(username)
            if not user:
                user_model = UserModel(username=username)
                user_dict = user_model.dict()
                print(f"[+] Create User {user_model.username}:{user_model.password}")
                user = self.gl.users.create(user_dict)
            # Assign groups
            groups = user_data.get("groups", None)
            if groups is None:
                continue
            for group_name, group_access in groups.items():
                # WARNING: not guaranteed to assign permission to the correct group
                # (duplicate names are hard)
                group = self.get_group(group_name)
                if not group:
                    print(f"[!] Group not found: {group_name}")
                    continue
                member_model = MemberModel(user_id=user.id, access_level_name=group_access)
                member_dict = member_model.dict()
                try:
                    group.members.create(member_dict)
                    print(f"[+] Grant {user.username} {group_access} on {group.name}")
                except gitlab.GitlabCreateError as e:
                    pass
            # Add keys
            self.add_keys(user, user_data.get("keys", {}))

    def found(self, obj_list):
        if not isinstance(obj_list, list):
            obj_list = [obj_list]
        for obj in obj_list:
            if obj is not None:
                msg = f"[*] Found {obj.__class__.__name__}"
                if hasattr(obj, "name_with_namespace"):
                    msg += f" {obj.name_with_namespace}"
                elif hasattr(obj, "name"):
                    msg += f" {obj.name}"
                if hasattr(obj, "id"):
                    msg += f" (id={obj.id})"
                print(msg)
            return obj
            
    def delete_groups(self, groups, parent_id=None):
        if groups is None:
            return

        for group_name, group_data in groups.items():
            if group_data is None:
                group_data = {}
            group = self.get_group(group_name, parent_id=parent_id)
            if group is not None:
                self.delete_groups(group_data.get("groups", None), parent_id=group.id)
                self.delete_projects(group_data.get("projects", None), namespace_id=group.id)
                # Do not delete protected groups
                if not group_data.get("protected", False):
                    self.delete_existing(group)
            print("[*] ...")

    def delete_projects(self, projects, namespace_id=None):
        if projects is None:
            return

        for project_name, project_data in projects.items():
            project = self.get_project(project_name, parent_id=namespace_id)
            self.delete_existing(project)

    def delete_users(self, spec):
        for username, user_data in spec.get("users", {}).items():
            user = self.get_user(username)
            self.delete_existing(user)

    def delete_existing(self, obj_list):
        # Warning: API returns success before delete is finished
        if not isinstance(obj_list, list):
            obj_list = [obj_list]

        for obj in obj_list:
            if obj is None:
                continue
            msg = f"[-] Delete {obj.__class__.__name__}"
            if hasattr(obj, "name_with_namespace"):
                msg += f" {obj.name_with_namespace}"
            elif hasattr(obj, "name"):
                msg += f" {obj.name}"
            print(msg)
            try:
                obj.delete() 
            except Exception as e:
                msg = f"[!] Exception deleting {obj.__class__.__name__}"
                if hasattr(obj, "name"):
                    msg += f" {obj.name}"
                print(msg + f" -- ({type(e)}): {e}")

    def delete_all(self, specs):
        for spec_path in specs:
            with open(spec_path, "r") as spec_file:
                spec = yaml.safe_load(spec_file)
            self.delete_deploy(spec.get("deploy", {}))
            self.delete_groups(spec.get("groups", None))
            self.delete_users(spec)

    def require_admin_creds(self):
        # the /admin area implements some things that are not exposed to the API :/
        msg = f"[!] This operation requires GitLab login credentials for an Administrator."
        if self.config.admin_user is None:
            print(msg)
            self.config.admin_user = getpass.getpass("user: ")
        if self.config.admin_password is None:
            print(msg)
            self.config.admin_password = getpass.getpass("password: ")
        
    def delete_deploy(self, deploy_data):
        keys = deploy_data.get("keys", {})

        for title, key_data in deploy_data.get("keys", {}).items():
            pub_key = key_data["key"]
            found = self.found(self.filter(self.gl.deploykeys.list(all=True), title=title))
            if found:
                self.delete_global_deploy_key(found)

    def process_global(self, global_model):
        if global_model.allow_local_requests:
            self.set_allow_local_requests(global_model.allow_local_requests)
        self.process_deploy(global_model.deploy)

    def process_deploy(self, deploy):
        for title, key in deploy.keys.items():
            if not key.title:
                key.title = title
            found = self.found(self.filter(self.gl.deploykeys.list(all=True), title=key.title, key=key.key))
            if found:
                self.global_keys[found.id] = key
            else:
                self.global_deploy_key(key)
            
    def run(self, specs):
        for spec_path in specs:
            with open(spec_path, "r") as spec_file:
                spec = yaml.safe_load(spec_file)
            self.spec_curr = spec
            self.spec_global_curr = GlobalModel.parse_obj(spec.get("global", {}))
            self.process_global(self.spec_global_curr)

            self.create_groups(spec.get("groups", None))
            self.create_users(spec)

            # Reset curr
            self.replica_curr = self.replica_spec

    def close(self):
        self.gl.__exit__()

    def export_spec(self, path):
        groups_map = {}
        groups = self.gl.groups.list(all=True)

        for g in groups:
            print(f"group: {g.id} | {g.path} | parent: {g.parent_id} | children: {g.subgroups.list()} ")
        # TODO: This is very dumb / inefficient
        spec = {}
        for parent in [g for g in groups if g.parent_id is None]:
            spec[parent.path] = self.get_children(parent, groups)
        
        spec = {"groups": spec}
        data = yaml.safe_dump(spec, default_flow_style=False)
        Path(path).write_text(data)
        print(f"[+] Exported spec to {path}")

    def get_children(self, parent, groups):
        parent_id = None
        if parent is not None:
            parent_id = parent.id
        children = {g.path : self.get_children(g, groups) for g in groups if g.parent_id == parent_id}
        print(f"[*] Visit {parent_id}: {list(children)}")
        if children:
            children = {"groups": children}
        projects = {p.path: {} for p in parent.projects.list()}
        if projects:
            children["projects"] = projects
        return children
            
    
    @classmethod
    def load_config(self, path, encrypted=False):
        data = Path(path).read_bytes()
        if encrypted:
            data = decrypt_data(data)
        return yaml.safe_load(data.decode())

    @classmethod
    def write_config(self, config, path, encrypted=False):
        data = yaml.safe_dump(config, default_flow_style=False).encode()
        if encrypted:
            data = encrypt_data(data)
        Path(path).write_bytes(data)
        
    @classmethod
    def get_parser(self):
        parser = argparse.ArgumentParser(prog="autogit")
        parser.add_argument("-c", "--config",
            required=True,
            help="Path to autogitlab config"
        )
        parser.add_argument("-e", "--encrypted",
            action="store_true",
            help="Set if config is encrypted"
        )
        parser.add_argument("--delete-all",
            action="store_true",
            help="[WARNING: DANGER] Set to delete all objects defined in spec(s)"
        )
        parser.add_argument("--debug-shell",
            action="store_true",
            help="[WARNING: DANGER] Drop into a debug shell"
        )
        parser.add_argument("--out-encrypted-config",
            help="If given, write config to an encrypted file and exit"
        )
        parser.add_argument("--init-root-token",
            action="store_true",
            help="Prompt for the initial root password, add a personal access token, and exit"
        )
        parser.add_argument("--out-replica-spec",
            help="If given, write a spec file that is suitable for setting up a secondary, push mirror instance for the provided spec(s)"
        )
        parser.add_argument("--out-replica-group",
            help="If given, encapsulate all groups within this base group. Use with --out-replica-spec"
        )
        parser.add_argument("--out-export-spec",
            help="If given, enumerate an existing instance, generate a corresponding spec, and exit"
        )
        parser.add_argument("spec",
            nargs="*",
            help="autogitlab specs to apply"
        )
        return parser

parser = AutoGit.get_parser()
args = parser.parse_args()
config = AutoGit.load_config(args.config, encrypted=args.encrypted)

auto_git = AutoGit(config=config)

if args.debug_shell:
    import IPython
    IPython.embed()

if args.init_root_token:
    auto_git.config.admin_user = getpass.getpass("user: ")
    auto_git.config.admin_password = getpass.getpass("password: ")
    auto_git.config.private_token = auto_git.password_to_token()
    # Reload with new config
    auto_git = AutoGit(config=auto_git.config.dict())

if args.out_encrypted_config:
    AutoGit.write_config(auto_git.config.dict(), args.out_encrypted_config, encrypted=True)
elif args.out_export_spec:
    auto_git.export_spec(args.out_export_spec)
elif not args.spec:
    print("[-] No specs given")
elif args.delete_all:
    auto_git.delete_all(args.spec)
else:
    auto_git.run(args.spec)
    if args.out_replica_spec is not None:
        spec = auto_git.replica_spec
        if args.out_replica_group is not None:
            spec = {
                "groups": {
                    args.out_replica_group: spec
                }
            }
        spec_yaml = yaml.safe_dump(spec, default_flow_style=False)
        Path(args.out_replica_spec).write_text(spec_yaml)
        print(f"[+] Replica spec written to {args.out_replica_spec}")

auto_git.close()
