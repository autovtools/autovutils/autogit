#!/bin/bash

docker exec -it docker_gitlab_1 cat /etc/gitlab/initial_root_password | grep Password: | awk '{print $2}'
